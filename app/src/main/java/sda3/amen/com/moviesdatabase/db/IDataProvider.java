package sda3.amen.com.moviesdatabase.db;

import java.util.List;

import sda3.amen.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 5/30/17.
 */

public interface IDataProvider {

    List<Movie> getAllMovies();
    void saveAllMovies(List<Movie> list);
}
