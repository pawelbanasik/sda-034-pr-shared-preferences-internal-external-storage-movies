package sda3.amen.com.moviesdatabase.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import sda3.amen.com.moviesdatabase.db.InternalDatabaseDataProvider;

/**
 * Created by RENT on 2017-06-08.
 */

public class MoviesContentProvider extends ContentProvider {

    public static final String PROVIDER_NAME =
            "sda3.amen.com.moviesdatabase.providers.MoviesContentProvider";
    public static final String PROVIDER_URI = "content://" + PROVIDER_NAME + "/movies";

    public static final Uri uri = Uri.parse(PROVIDER_URI);

    @Override
    public boolean onCreate() {
        Context context = getContext();
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(context);
        SQLiteDatabase db = provider.getReadableDatabase();

        if (db != null) {
            return true;
        }

        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();
        Cursor cursor = db.query(InternalDatabaseDataProvider.getDbTableName(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getWritableDatabase();
        long id = db.insert(InternalDatabaseDataProvider.getDbTableName(),
                null,
                values);


        return uri.parse(PROVIDER_URI + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();

        return db.delete(InternalDatabaseDataProvider.getDbTableName(),
                selection,
                selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getWritableDatabase();
        return db.update(InternalDatabaseDataProvider.getDbTableName(), values, selection, selectionArgs);
    }
}
