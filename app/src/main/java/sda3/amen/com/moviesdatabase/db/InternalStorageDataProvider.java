package sda3.amen.com.moviesdatabase.db;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import sda3.amen.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 5/31/17.
 */

public class InternalStorageDataProvider implements IDataProvider {
    private static final String FILE_PATH = "internal_storage.txt";
    private Context context;

    public InternalStorageDataProvider(Context context) {
        this.context = context;
    }

    @Override
    public List<Movie> getAllMovies() {
        List<Movie> returnedMoviesList = new ArrayList<>();

        try {
            FileInputStream fis = context.openFileInput(FILE_PATH);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            // przeczytaj linie, jesli nie bedzie niczego na wejsciu to buffered reader zwróci
            // null z metody readLine
            String line = bufferedReader.readLine();

            // dopoki linia nie bedzie pusta
            while (line != null) {
                try {
                    Movie singleMovie = new Movie(line);
                    returnedMoviesList.add(singleMovie);
                } catch (ParseException pe) {
                    Log.e(getClass().getName(), pe.getMessage());
                }
                // czytaj nastepna linie
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }

        return returnedMoviesList;
    }

    @Override
    public void saveAllMovies(List<Movie> list) {
        try {
            FileOutputStream fos = context.openFileOutput(FILE_PATH, Context.MODE_PRIVATE);
            PrintWriter printWriter = new PrintWriter(fos);

            for (Movie singleMovie : list) {
                printWriter.println(singleMovie.toDatabaseFormat());
            }

            printWriter.close();
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }
    }
}
