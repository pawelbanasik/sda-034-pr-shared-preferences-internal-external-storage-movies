package sda3.amen.com.moviesdatabase.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import sda3.amen.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 6/5/17.
 */

public class InternalDatabaseDataProvider extends SQLiteOpenHelper implements IDataProvider {

    // stałe wersji bazy oraz nazwy bazy
    private static final String DB_NAME = "movies.db";
    private static final int DB_VERSION = 1;

    // stałe - nazwy kolumn i tabeli
    private static final String DB_TABLE_NAME = "movies";

    // kolumny
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DURATION = "duration";

    public InternalDatabaseDataProvider(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_TABLE_NAME + " ( " +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " VARCHAR, " +
                COLUMN_DURATION + " INTEGER); "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        onCreate(db);
    }

    // insert into DB_NAME ( KOLUMNY) VALUES (WARTOSCI);
    private void insertRecordIntoDatabase(Movie movie) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO " + DB_TABLE_NAME + " (" +
                COLUMN_ID + ", " +
                COLUMN_NAME + ", " +
                COLUMN_DURATION + ") " +
                " VALUES ( NULL, '" + movie.getTitle() + "', " + movie.getDuration() + ")");
    }

    @Override
    public List<Movie> getAllMovies() {
        List<Movie> list = new LinkedList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DB_TABLE_NAME, null);

        // iterujemy kursor, przechadzamy się między wierszami, więc na początek
        // trzeba przejść do pierwszego (move to next)
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();

            // pobieram 3 wartosci
            int id = cursor.getInt(0);
            String title = cursor.getString(1);
            int duration = cursor.getInt(2);

            // tworze obiekt, ustawiam 3 wartości
            Movie newMovie = new Movie();
            newMovie.setId(id);
            newMovie.setTitle(title);
            newMovie.setDuration(duration);

            // dodaje do listy
            list.add(newMovie);
        }

        return list;
    }

    @Override
    public void saveAllMovies(List<Movie> list) {
        for (Movie m : list) {
            if (m.getId() == -1) {
                insertRecordIntoDatabase(m);
            }
        }
    }

    public static String getDbTableName() {
        return DB_TABLE_NAME;
    }
}
